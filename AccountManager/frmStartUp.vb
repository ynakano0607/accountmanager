﻿Public Class frmStartUp
    Private Sub frmStartUp_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'frmMainを呼び出すだけのフォーム
        'frmMainをエントリポイントにすると再ロードができないので
        Dim _frmMain As New frmMain()

        With _frmMain
            .Owner = Me
            .ShowDialog()
        End With

        Me.Hide()
    End Sub
End Class