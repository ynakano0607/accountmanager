﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModifyService
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmModifyService))
        Me.lblServiceIDHeader = New System.Windows.Forms.Label()
        Me.lblServiceNameHeader = New System.Windows.Forms.Label()
        Me.lblURLHeader = New System.Windows.Forms.Label()
        Me.lblServiceDescriptionHeader = New System.Windows.Forms.Label()
        Me.lblServiceID = New System.Windows.Forms.Label()
        Me.txtServiceName = New System.Windows.Forms.TextBox()
        Me.txtURL = New System.Windows.Forms.TextBox()
        Me.txtServiceDescription = New System.Windows.Forms.TextBox()
        Me.cmdRegisterSerivice = New System.Windows.Forms.Button()
        Me.cmdCansel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblServiceIDHeader
        '
        Me.lblServiceIDHeader.AutoSize = True
        Me.lblServiceIDHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceIDHeader.Location = New System.Drawing.Point(92, 9)
        Me.lblServiceIDHeader.Name = "lblServiceIDHeader"
        Me.lblServiceIDHeader.Size = New System.Drawing.Size(49, 19)
        Me.lblServiceIDHeader.TabIndex = 0
        Me.lblServiceIDHeader.Text = "ID："
        '
        'lblServiceNameHeader
        '
        Me.lblServiceNameHeader.AutoSize = True
        Me.lblServiceNameHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceNameHeader.Location = New System.Drawing.Point(12, 38)
        Me.lblServiceNameHeader.Name = "lblServiceNameHeader"
        Me.lblServiceNameHeader.Size = New System.Drawing.Size(129, 19)
        Me.lblServiceNameHeader.TabIndex = 1
        Me.lblServiceNameHeader.Text = "サービス名："
        '
        'lblURLHeader
        '
        Me.lblURLHeader.AutoSize = True
        Me.lblURLHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblURLHeader.Location = New System.Drawing.Point(82, 72)
        Me.lblURLHeader.Name = "lblURLHeader"
        Me.lblURLHeader.Size = New System.Drawing.Size(59, 19)
        Me.lblURLHeader.TabIndex = 2
        Me.lblURLHeader.Text = "URL："
        '
        'lblServiceDescriptionHeader
        '
        Me.lblServiceDescriptionHeader.AutoSize = True
        Me.lblServiceDescriptionHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceDescriptionHeader.Location = New System.Drawing.Point(32, 151)
        Me.lblServiceDescriptionHeader.Name = "lblServiceDescriptionHeader"
        Me.lblServiceDescriptionHeader.Size = New System.Drawing.Size(109, 19)
        Me.lblServiceDescriptionHeader.TabIndex = 3
        Me.lblServiceDescriptionHeader.Text = "コメント："
        '
        'lblServiceID
        '
        Me.lblServiceID.AutoSize = True
        Me.lblServiceID.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceID.Location = New System.Drawing.Point(135, 9)
        Me.lblServiceID.Name = "lblServiceID"
        Me.lblServiceID.Size = New System.Drawing.Size(59, 19)
        Me.lblServiceID.TabIndex = 4
        Me.lblServiceID.Text = "dummy"
        '
        'txtServiceName
        '
        Me.txtServiceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtServiceName.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtServiceName.Location = New System.Drawing.Point(139, 36)
        Me.txtServiceName.MaxLength = 50
        Me.txtServiceName.Name = "txtServiceName"
        Me.txtServiceName.Size = New System.Drawing.Size(177, 26)
        Me.txtServiceName.TabIndex = 5
        '
        'txtURL
        '
        Me.txtURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtURL.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtURL.Location = New System.Drawing.Point(139, 70)
        Me.txtURL.MaxLength = 255
        Me.txtURL.Multiline = True
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(341, 73)
        Me.txtURL.TabIndex = 6
        Me.txtURL.Text = "http://"
        '
        'txtServiceDescription
        '
        Me.txtServiceDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtServiceDescription.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtServiceDescription.Location = New System.Drawing.Point(139, 149)
        Me.txtServiceDescription.MaxLength = 255
        Me.txtServiceDescription.Multiline = True
        Me.txtServiceDescription.Name = "txtServiceDescription"
        Me.txtServiceDescription.Size = New System.Drawing.Size(341, 73)
        Me.txtServiceDescription.TabIndex = 7
        '
        'cmdRegisterSerivice
        '
        Me.cmdRegisterSerivice.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdRegisterSerivice.Location = New System.Drawing.Point(16, 236)
        Me.cmdRegisterSerivice.Name = "cmdRegisterSerivice"
        Me.cmdRegisterSerivice.Size = New System.Drawing.Size(128, 33)
        Me.cmdRegisterSerivice.TabIndex = 8
        Me.cmdRegisterSerivice.Text = "登録(&R)"
        Me.cmdRegisterSerivice.UseVisualStyleBackColor = True
        '
        'cmdCansel
        '
        Me.cmdCansel.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCansel.Location = New System.Drawing.Point(152, 237)
        Me.cmdCansel.Name = "cmdCansel"
        Me.cmdCansel.Size = New System.Drawing.Size(128, 33)
        Me.cmdCansel.TabIndex = 9
        Me.cmdCansel.Text = "キャンセル(&C)"
        Me.cmdCansel.UseVisualStyleBackColor = True
        '
        'frmModifyService
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(492, 283)
        Me.Controls.Add(Me.cmdCansel)
        Me.Controls.Add(Me.cmdRegisterSerivice)
        Me.Controls.Add(Me.txtServiceDescription)
        Me.Controls.Add(Me.txtURL)
        Me.Controls.Add(Me.txtServiceName)
        Me.Controls.Add(Me.lblServiceID)
        Me.Controls.Add(Me.lblServiceDescriptionHeader)
        Me.Controls.Add(Me.lblURLHeader)
        Me.Controls.Add(Me.lblServiceNameHeader)
        Me.Controls.Add(Me.lblServiceIDHeader)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmModifyService"
        Me.Text = "Account Manager / サービス"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblServiceIDHeader As Label
    Friend WithEvents lblServiceNameHeader As Label
    Friend WithEvents lblURLHeader As Label
    Friend WithEvents lblServiceDescriptionHeader As Label
    Friend WithEvents lblServiceID As Label
    Friend WithEvents txtServiceName As TextBox
    Friend WithEvents txtURL As TextBox
    Friend WithEvents txtServiceDescription As TextBox
    Friend WithEvents cmdRegisterSerivice As Button
    Friend WithEvents cmdCansel As Button
End Class
