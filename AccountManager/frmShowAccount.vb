﻿Imports System.Runtime.InteropServices  'DLLのインポートに必要な宣言

Public Class frmShowAccount
    '閉じるボタンの無効化
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_NOCLOSE As Integer = &H200
            Dim cp As CreateParams = MyBase.CreateParams

            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property

    'ShowWindow関数の定義
    <DllImport("USER32.DLL", CharSet:=CharSet.Auto)>
    Private Shared Function _
             ShowWindow(ByVal hWnd As Integer, ByVal nCmdShow As Integer) As Boolean
    End Function

    '呼出元フォームから値を受取るためのプロパティ
    Public Property TargetServiceID As String   '編集対象のID番号

    'データテーブルの定義
    Private ServiceIDDT As New DataTable()  'tbl_serviceの全レコード
    Private AccountDT As New DataTable()    'tbl_accountの全レコード
    Private MaxIDDT As New DataTable()      'tbl_accountの対象サービスIDの最大アカウントID

    '動的配置コントロールの定義
    Private lblAccountTable() As System.Windows.Forms.Label
    Private cmdOpenBrowser() As System.Windows.Forms.Button
    Private cmdModifyAccount() As System.Windows.Forms.Button
    Private cmdDeleteAccount() As System.Windows.Forms.Button
    Private cmdAddAccount() As System.Windows.Forms.Button

    Private Sub frmShowAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.lblServiceID.Text = TargetServiceID

        Dim strSQL_Service As String            'SQL文字列
        Dim strSQL_Account As String
        Dim strSQL_MAX As String

        Dim i As Integer    'レコード数ループカウンタ
        Dim j As Integer    'カラム数ループカウンタ
        Dim LabelID As Integer = 1  'ラベルID

        strSQL_Service = "SELECT ServiceName,URL,ServiceDescription FROM tbl_service WHERE ServiceID = " & Integer.Parse(TargetServiceID)
        strSQL_MAX = "SELECT MAX(AccountID) FROM tbl_account WHERE ServiceID = " & Integer.Parse(TargetServiceID)
        strSQL_Account = "SELECT AccountID,ID1,ID2,Password1,Password2,AccountDescription FROM tbl_account WHERE ServiceID =" &
                            Integer.Parse(TargetServiceID)

        'サービス部分の取得
        Dim SqlDA_Service As New SqlClient.SqlDataAdapter(strSQL_Service, clsCommonValues.strSqlConnection)
        Dim sqlDA_MAX As New SqlClient.SqlDataAdapter(strSQL_MAX, clsCommonValues.strSqlConnection)

        Try
            SqlDA_Service.Fill(ServiceIDDT)
            sqlDA_MAX.Fill(MaxIDDT)
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try

        'フォームのタイトル設定
        Me.Text &= ServiceIDDT.Rows(0).Item(0).ToString()

        Me.lblServiceName.Text = ServiceIDDT.Rows(0).Item(0).ToString()
        Me.lllURL.Text = ServiceIDDT.Rows(0).Item(1).ToString()
        Me.lblServiceDescription.Text = ServiceIDDT.Rows(0).Item(2).ToString()

        'アカウント部分の取得
        Dim SqlDA_Account As New SqlClient.SqlDataAdapter(strSQL_Account, clsCommonValues.strSqlConnection)

        Try
            SqlDA_Account.Fill(AccountDT)
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try

        '動的配置コントロールの生成
        Me.lblAccountTable = New System.Windows.Forms.Label(AccountDT.Rows.Count * AccountDT.Columns.Count) {}
        Me.cmdOpenBrowser = New System.Windows.Forms.Button(AccountDT.Rows.Count) {}
        Me.cmdModifyAccount = New System.Windows.Forms.Button(AccountDT.Rows.Count) {}
        Me.cmdDeleteAccount = New System.Windows.Forms.Button(AccountDT.Rows.Count) {}
        Me.cmdAddAccount = New System.Windows.Forms.Button(1) {}

        For i = 0 To AccountDT.Rows.Count - 1
            For j = 0 To AccountDT.Columns.Count - 1

                'サービスID/サービス名称ラベルの動的生成
                Dim ObjWidth As Integer
                Dim ObjHeight As Integer
                Dim ObjXPos As Integer
                Dim ObjYPos As Integer

                Select Case j   'テーブル内容表示ラベルの大きさと表示位置
                    Case 0
                        ObjWidth = 35
                        ObjHeight = 30
                        ObjXPos = i * 120 + 60
                        ObjYPos = 0
                    Case 1
                        ObjWidth = 350
                        ObjHeight = 30
                        ObjXPos = i * 120 + 60
                        ObjYPos = 35
                    Case 2
                        ObjWidth = 350
                        ObjHeight = 30
                        ObjXPos = i * 120 + 60
                        ObjYPos = 385
                    Case 3
                        ObjWidth = 350
                        ObjHeight = 30
                        ObjXPos = i * 120 + 90
                        ObjYPos = 35
                    Case 4
                        ObjWidth = 350
                        ObjHeight = 30
                        ObjXPos = i * 120 + 90
                        ObjYPos = 385
                    Case 5
                        ObjWidth = 700
                        ObjHeight = 60
                        ObjXPos = i * 120 + 120
                        ObjYPos = 35
                End Select

                Me.lblAccountTable(LabelID) = New System.Windows.Forms.Label
                With Me.lblAccountTable(LabelID)
                    .Name = "lblAccountTable" + LabelID.ToString()
                    .Text = AccountDT.Rows(i).Item(j).ToString()
                    .Size = New Size(ObjWidth, ObjHeight)
                    .Location = New Point(ObjYPos, ObjXPos)
                    .Font = New Font("MSゴシック", 11)
                    .BorderStyle = BorderStyle.FixedSingle
                End With

                LabelID += 1
            Next

            'ブラウザを開くボタンの動的生成
            Me.cmdOpenBrowser(i) = New System.Windows.Forms.Button
            With Me.cmdOpenBrowser(i)
                .Name = "cmdOpenBrowser" + (i + 1).ToString()    'オブジェクト名は1から開始
                .Text = "ブラウザ自動入力"
                .Size = New Size(120, 30)
                .Location = New Point(750, i * 120 + 60)
                .Font = New Font("MSゴシック", 10)
            End With
            AddHandler Me.cmdOpenBrowser(i).Click, AddressOf cmdOpenBrowser_Click 'イベントハンドラ

            'エントリ修正ボタンの動的生成
            Me.cmdModifyAccount(i) = New System.Windows.Forms.Button
            With Me.cmdModifyAccount(i)
                .Name = "cmdModifyAccount" + (i + 1).ToString()    'オブジェクト名は1から開始
                .Text = "エントリ修正"
                .Size = New Size(120, 30)
                .Location = New Point(750, i * 120 + 90)
                .Font = New Font("MSゴシック", 10)
            End With
            AddHandler Me.cmdModifyAccount(i).Click, AddressOf cmdModifyAccount_Click 'イベントハンドラ

            '削除ボタンの動的生成
            Me.cmdDeleteAccount(i) = New System.Windows.Forms.Button
            With Me.cmdDeleteAccount(i)
                .Name = "cmdDeleteAccount" + (i + 1).ToString()    'オブジェクト名は1から開始
                .Text = "削除"
                .Size = New Size(120, 30)
                .Location = New Point(750, i * 120 + 120)
                .Font = New Font("MSゴシック", 10)
            End With
            AddHandler Me.cmdDeleteAccount(i).Click, AddressOf cmdDeleteAccount_Click 'イベントハンドラ

        Next

        'アカウント追加ボタンの動的生成
        Me.cmdAddAccount(0) = New System.Windows.Forms.Button
        With Me.cmdAddAccount(0)
            .Name = "cmdAddAccount"
            .Text = "アカウント追加"
            .Size = New Size(120, 30)
            .Location = New Point(50, i * 120 + 70)
            .Font = New Font("MSゴシック", 10)
        End With
        AddHandler Me.cmdAddAccount(0).Click, AddressOf cmdAddAccount_Click 'イベントハンドラ

        '動的生成コントロールをフォームに配置
        With Me.SplitContainer1.Panel2.Controls
            .AddRange(Me.lblAccountTable)
            .AddRange(Me.cmdOpenBrowser)
            .AddRange(Me.cmdModifyAccount)
            .AddRange(Me.cmdDeleteAccount)
            .AddRange(Me.cmdAddAccount)
        End With

        'データアダプタの開放
        SqlDA_Service.Dispose()
        sqlDA_MAX.Dispose()
        SqlDA_Account.Dispose()

        'フォームが4つ以上ある場合、アイテム番号2(つまり3番目)のフォームは「古い」frmShowAccountフォームと
        'みなしてクローズする(frmModifyAccountから再ロードさせたい)
        If My.Application.OpenForms.Count > 3 Then
            My.Application.OpenForms.Item(2).Dispose()
        End If
    End Sub

    Private Sub cmdOpenBrowser_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const PREFIX_LENGTH As Integer = 14 '押下されたボタン名から除去する文字数「cmdOpenBrowser」

        Dim AccountID As Integer

        '押下されたボタンからアカウント番号を取得
        AccountID = Integer.Parse(sender.name.ToString().Substring(PREFIX_LENGTH))

        'URLリンクラベルを訪問済みカラーにする
        Me.lllURL.LinkVisited = True

        'IEの各レベルのCOMオブジェクトを全て宣言しておく必要がある
        '全て宣言して全て解放しないと2度目以降に実行した際に例外が発生する
        Dim ObjIE As SHDocVw.InternetExplorer =
            CType(CreateObject("InternetExplorer.application"), SHDocVw.InternetExplorer)   'IEオブジェクト
        Dim ObjIEDocument As mshtml.HTMLDocument                                            'IEドキュメントオブジェクト
        Dim ObjIEDocumentInputTag As mshtml.IHTMLElementCollection                          '"input"タグオブジェクトのコレクション
        Dim ObjIEForm As mshtml.HTMLFormElement                                             'フォームオブジェクト
        Dim ObjTag As mshtml.HTMLInputElement                                               '"input"タグオブジェクト

        ObjIE.Visible = True
        ObjIE.Navigate(Me.lllURL.Text)

        Dim r As Boolean = ShowWindow(ObjIE.HWND, 3)    'ウィンドウの最大化（第二引数の"3"が最大化）

        Try
            'ページの表示完了を待つ ReadyState="4"は読み込み完了
            While ObjIE.ReadyState <> 4 Or ObjIE.Busy = True
                System.Windows.Forms.Application.DoEvents() '他の処理にリソースを開放
                System.Threading.Thread.Sleep(1000)         '1秒スリープ
            End While

            ObjIEDocument = CType(ObjIE.Document, mshtml.HTMLDocument)
            ObjIEDocumentInputTag = CType(ObjIEDocument.getElementsByTagName("input"), mshtml.IHTMLElementCollection)
            ObjIEForm = CType(ObjIEDocument.forms(0), mshtml.HTMLFormElement)

            'フォームのテキストボックスへの値のセット
            If ObjIEDocumentInputTag.length > 0 Then
                For Each ObjTag In ObjIEDocumentInputTag
                    If ObjTag.type.ToString() = "text" Or ObjTag.type.ToString() = "email" Then
                        ObjTag.value = Me.lblAccountTable((AccountID - 1) * 6 + 2).Text
                    ElseIf ObjTag.type.ToString() = "password" Then
                        ObjTag.value = Me.lblAccountTable((AccountID - 1) * 6 + 4).Text
                    End If
                Next

                'フォームの送信
                ObjIEForm.submit()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        Finally
            '全てのCOMオブジェクトを解放
            If ObjTag IsNot Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ObjTag)
                ObjTag = Nothing
            End If

            If ObjIEForm IsNot Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ObjIEForm)
                ObjIEForm = Nothing
            End If

            If ObjIEDocumentInputTag IsNot Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ObjIEDocumentInputTag)
                ObjIEDocumentInputTag = Nothing
            End If

            If ObjIEDocument IsNot Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ObjIEDocument)
                ObjIEDocument = Nothing
            End If

            If ObjIE IsNot Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ObjIE)
                ObjIE = Nothing
            End If

            '念のためガベッジコレクション
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

    Private Sub cmdModifyAccount_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const PREFIX_LENGTH As Integer = 16 '押下されたボタン名から除去する文字数「cmdModifyAccount」

        Dim ObjectID As Integer     '削除対象レコードのフォーム上のオブジェクト番号
        Dim AccountID As Integer    '削除対象レコードのDBのID番号

        ObjectID = Integer.Parse(sender.name.ToString().Substring(PREFIX_LENGTH))
        AccountID = Integer.Parse(Me.lblAccountTable((ObjectID - 1) * 6 + 1).Text)

        Dim _frmModifyAccount As New frmModifyAccount()

        With _frmModifyAccount
            .TargetServiceID = TargetServiceID
            .TargetAccountID = AccountID.ToString()
            .TargetServiceName = Me.lblServiceName.Text
            .OpenMode = "MODIFY"
            .ShowDialog()
        End With
    End Sub

    Private Sub cmdDeleteAccount_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const PREFIX_LENGTH As Integer = 16 '押下されたボタン名から除去する文字数「cmdDeleteAccount」

        Dim ObjectID As Integer     '削除対象レコードのフォーム上のオブジェクト番号
        Dim AccountID As Integer    '削除対象レコードのDBのID番号

        Dim strSQL As String    'SQL文字列

        ObjectID = Integer.Parse(sender.name.ToString().Substring(PREFIX_LENGTH))
        AccountID = Integer.Parse(Me.lblAccountTable((ObjectID - 1) * 6 + 1).Text)

        If MessageBox.Show("ID : " & AccountID & vbCrLf & "ユーザID(1) : " & lblAccountTable((ObjectID - 1) * 6 + 2).Text & vbCrLf &
            "を削除します よろしいですか？", "削除の確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbNo Then
            MessageBox.Show("削除処理は行いません", "処理中断", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        strSQL = "DELETE FROM tbl_account WHERE ServiceID =" & TargetServiceID & "AND AccountID =" & AccountID

        'DB接続した後一度Fillしたデータテーブルに更新をする必要がある
        Dim SqlDA As New SqlClient.SqlDataAdapter(strSQL, clsCommonValues.strSqlConnection)

        Try
            SqlDA.Fill(ServiceIDDT)
            SqlDA.Update(ServiceIDDT)
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

        'データアダプタの開放
        SqlDA.Dispose()

        MessageBox.Show("ID : " & AccountID & "は削除されました", "処理成功", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'このフォームを開き直す
        Me.Dispose()    '「ここでは」先にDisposeする必要がある

        Dim _frmShowAccount As New frmShowAccount()

        With _frmShowAccount
            .TargetServiceID = TargetServiceID
            .ShowDialog()
        End With
    End Sub

    Private Sub cmdAddAccount_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim _frmModifyAccount As New frmModifyAccount()

        '呼出先に引き渡すプロパティ
        With _frmModifyAccount
            If DBNull.Value.Equals(MaxIDDT.Rows(0).Item(0)) Then    'レコードがない(null)場合
                .TargetAccountID = "1"
            Else
                .TargetAccountID = (MaxIDDT.Rows(0).Item(0) + 1).ToString()
            End If

            .TargetServiceID = TargetServiceID
            .TargetServiceName = Me.lblServiceName.Text
            .OpenMode = "ADD"
            .ShowDialog()
        End With
    End Sub

    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Dispose()
    End Sub

    Private Sub lllURL_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lllURL.LinkClicked
        Dim ObjIE As Object

        Me.lllURL.LinkVisited = True

        ObjIE = CreateObject("InternetExplorer.application")
        ObjIE.Visible = True
        ObjIE.Navigate(Me.lllURL.Text)
    End Sub

    '"System.__ComObject"の実際の名称を知る方法
    'MessageBox.Show(Microsoft.VisualBasic.Information.TypeName(ObjIEDocument.getElementsByTagName("input")))
End Class