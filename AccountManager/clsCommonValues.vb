﻿Imports System.ServiceProcess

Public Class clsCommonValues
    'DB接続文字列
    Public Const strSqlConnection As String = "Data Source=MAYUMI-PC\SQLEXPRESS;Initial Catalog=AccountManagerDB;Integrated Security=True"

    Public Sub SQLServiceControl()
        Dim sc As New ServiceController("MSSQL$SQLEXPRESS")

        If sc.Status = ServiceControllerStatus.Stopped Then
            Try
                MessageBox.Show("SQL Serverが起動していません" & vbCrLf & "起動します", "SQL Server停止中",
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                sc.Start()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
                MessageBox.Show("アプリケーションを終了します", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Environment.Exit(-1)
            End Try
        End If
    End Sub

    'アンパサンド二重化処理(「&」一つではエスケープされるため）
    Public Function SetAnpersandDoubled(inStr As String) As String
        Return Replace(inStr, "&", "&&")
    End Function

    'アンパサンド縮減処理(上記関数の逆 表示用）
    Public Function SetAnpersandSingled(inStr As String) As String
        Return Replace(inStr, "&&", "&")
    End Function

    '文字列バイト数カウント
    'vb.netはUnicodeで文字列を保持しているので、一度CP932に変換してからバイト数を数える必要がある(らしい)
    Public Function getStringByteCount(inStr As String) As Integer
        Return System.Text.Encoding.GetEncoding(932).GetByteCount(inStr)
    End Function
End Class
