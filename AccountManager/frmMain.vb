﻿Public Class frmMain
    '閉じるボタンの無効化
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_NOCLOSE As Integer = &H200
            Dim cp As CreateParams = MyBase.CreateParams

            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property

    'データテーブルの定義
    Private ServiceIDDT As New DataTable()  'tbl_serviceの全レコード
    Private MaxIDDT As New DataTable()      'tbl_serviceのID番号の最大値

    '動的配置コントロールの定義
    Private lblServiceTable() As System.Windows.Forms.Label
    Private cmdOpenDetail() As System.Windows.Forms.Button
    Private cmdModifyDetail() As System.Windows.Forms.Button
    Private cmdDeleteService() As System.Windows.Forms.Button

    Private Sub frmMain(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strSQL_Service As String            'SQL文字列
        Dim strSQL_MAX As String

        Dim i As Integer    'レコード数ループカウンタ
        Dim j As Integer    'カラム数ループカウンタ
        Dim LabelID As Integer = 1  'ラベルID

        'SQL Server起動確認
        Dim _clsCommonValues As New clsCommonValues

        _clsCommonValues.SQLServiceControl()

        'SQL定義とデータアダプタ作成
        strSQL_Service = "SELECT ServiceID,ServiceName FROM tbl_service ORDER BY ServiceID"
        strSQL_MAX = "SELECT MAX(ServiceID) FROM tbl_service"

        Dim SqlDA_Service As New SqlClient.SqlDataAdapter(strSQL_Service, clsCommonValues.strSqlConnection)
        Dim sqlDA_MAX As New SqlClient.SqlDataAdapter(strSQL_MAX, clsCommonValues.strSqlConnection)

        Try
            SqlDA_Service.Fill(ServiceIDDT)
            sqlDA_MAX.Fill(MaxIDDT)

            '動的配置コントロールの生成
            Me.lblServiceTable = New System.Windows.Forms.Label(ServiceIDDT.Rows.Count * ServiceIDDT.Columns.Count) {}
            Me.cmdOpenDetail = New System.Windows.Forms.Button(ServiceIDDT.Rows.Count) {}
            Me.cmdModifyDetail = New System.Windows.Forms.Button(ServiceIDDT.Rows.Count) {}
            Me.cmdDeleteService = New System.Windows.Forms.Button(ServiceIDDT.Rows.Count) {}

            For i = 0 To ServiceIDDT.Rows.Count - 1
                For j = 0 To ServiceIDDT.Columns.Count - 1

                    'サービスID/サービス名称ラベルの動的生成
                    Me.lblServiceTable(LabelID) = New System.Windows.Forms.Label
                    With Me.lblServiceTable(LabelID)
                        .Name = "lblServiceTable" + LabelID.ToString()
                        .Text = ServiceIDDT.Rows(i).Item(j).ToString()
                        .Size = New Size(150, 30)
                        .Location = New Point(j * 150, i * 40)
                        .Font = New Font("MSゴシック", 14)
                        .BorderStyle = BorderStyle.FixedSingle
                    End With

                    LabelID += 1
                Next

                '詳細ボタンの動的生成
                Me.cmdOpenDetail(i) = New System.Windows.Forms.Button
                With Me.cmdOpenDetail(i)
                    .Name = "cmdOpenDetail" + (i + 1).ToString()    'オブジェクト名は1から開始
                    .Text = "詳細を開く"
                    .Size = New Size(90, 30)
                    .Location = New Point(j * 150, i * 40)
                    .Font = New Font("MSゴシック", 10)
                End With
                AddHandler Me.cmdOpenDetail(i).Click, AddressOf cmdOpenDetail_Click 'イベントハンドラ

                'サービス詳細修正ボタンの動的生成
                Me.cmdModifyDetail(i) = New System.Windows.Forms.Button
                With Me.cmdModifyDetail(i)
                    .Name = "cmdModifyDetail" + (i + 1).ToString()  'オブジェクト名は1から開始
                    .Text = "エントリ修正"
                    .Size = New Size(90, 30)
                    .Location = New Point(j * 195, i * 40)
                    .Font = New Font("MSゴシック", 10)
                End With
                AddHandler Me.cmdModifyDetail(i).Click, AddressOf cmdModifyDetail_Click 'イベントハンドラ

                'サービス削除ボタンの動的生成
                Me.cmdDeleteService(i) = New System.Windows.Forms.Button
                With Me.cmdDeleteService(i)
                    .Name = "cmdDeleteService" + (i + 1).ToString()   'オブジェクト名は1から開始
                    .Text = "削除"
                    .Size = New Size(90, 30)
                    .Location = New Point(j * 240, i * 40)
                    .Font = New Font("MSゴシック", 10)
                End With
                AddHandler Me.cmdDeleteService(i).Click, AddressOf cmdDeleteService_Click 'イベントハンドラ
            Next

            '動的生成コントロールをフォームに配置
            With Me.SplitContainer1.Panel1.Controls
                .AddRange(Me.lblServiceTable)
                .AddRange(Me.cmdOpenDetail)
                .AddRange(Me.cmdModifyDetail)
                .AddRange(Me.cmdDeleteService)
            End With
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.cmdAddservice.Enabled = False
        End Try

        'データアダプタの開放
        SqlDA_Service.Dispose()
        sqlDA_MAX.Dispose()

        'フォームが3つ以上ある場合、アイテム番号1(つまり2番目)のフォームは「古い」frmMainフォームと
        'みなしてクローズする(frmModifyServiceから再ロードさせたい)
        If My.Application.OpenForms.Count > 2 Then
            My.Application.OpenForms.Item(1).Dispose()
        End If
    End Sub

    Private Sub cmdOpenDetail_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const PREFIX_LENGTH As Integer = 13 '押下されたボタン名から除去する文字数「cmdOpenDetail」

        Dim ObjectID As Integer     '削除対象レコードのフォーム上のオブジェクト番号
        Dim ServiceID As Integer    '削除対象レコードのDBのID番号

        ObjectID = Integer.Parse(sender.name.ToString().Substring(PREFIX_LENGTH))
        ServiceID = Integer.Parse(Me.lblServiceTable(ObjectID * 2 - 1).Text)

        Dim _frmShowAccount As New frmShowAccount()

        With _frmShowAccount
            .TargetServiceID = ServiceID.ToString()
            .ShowDialog()
        End With
    End Sub

    Private Sub cmdModifyDetail_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const PREFIX_LENGTH As Integer = 15 '押下されたボタン名から除去する文字数「cmdModifyDetail」

        Dim ObjectID As Integer     '削除対象レコードのフォーム上のオブジェクト番号
        Dim ServiceID As Integer    '削除対象レコードのDBのID番号

        ObjectID = Integer.Parse(sender.name.ToString().Substring(PREFIX_LENGTH))
        ServiceID = Integer.Parse(Me.lblServiceTable(ObjectID * 2 - 1).Text)

        Dim _frmModifyService As New frmModifyService()

        With _frmModifyService
            .TargetServiceID = ServiceID.ToString()
            .OpenMode = "MODIFY"
            .ShowDialog()
        End With
    End Sub

    Private Sub cmdDeleteService_Click(ByVal sender As Object, ByVal e As EventArgs)
        Const PREFIX_LENGTH As Integer = 16 '押下されたボタン名から除去する文字数「cmdDeleteService」

        Dim ObjectID As Integer     '削除対象レコードのフォーム上のオブジェクト番号
        Dim ServiceID As Integer    '削除対象レコードのDBのID番号

        Dim strSQL As String    'SQL文字列

        ObjectID = Integer.Parse(sender.name.ToString().Substring(PREFIX_LENGTH))
        ServiceID = Integer.Parse(Me.lblServiceTable(ObjectID * 2 - 1).Text)

        If MessageBox.Show("ID : " & ServiceID & vbCrLf & "サービス名 : " & lblServiceTable(ObjectID * 2).Text & vbCrLf &
            "を削除します よろしいですか？", "削除の確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbNo Then
            MessageBox.Show("削除処理は行いません", "処理中断", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        strSQL = "DELETE FROM tbl_service WHERE ServiceID =" & ServiceID

        'DB接続した後一度Fillしたデータテーブルに更新をする必要がある
        Dim SqlDA As New SqlClient.SqlDataAdapter(strSQL, clsCommonValues.strSqlConnection)

        Try
            SqlDA.Fill(ServiceIDDT)
            SqlDA.Update(ServiceIDDT)
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

        'データアダプタの開放
        SqlDA.Dispose()

        MessageBox.Show("ID : " & ServiceID & "は削除されました", "処理成功", MessageBoxButtons.OK,
                        MessageBoxIcon.Information)

        'このフォームを開き直す
        Dim _frmMain As New frmMain()
        _frmMain.Show()

        Me.Dispose()
    End Sub

    Private Sub cmdAddservice_Click(sender As Object, e As EventArgs) Handles cmdAddservice.Click
        Dim _frmModifyService As New frmModifyService()

        '呼出先に引き渡すプロパティ
        With _frmModifyService
            If DBNull.Value.Equals(MaxIDDT.Rows(0).Item(0)) Then    'レコードがない(null)場合
                .TargetServiceID = "1"
            Else
                .TargetServiceID = (MaxIDDT.Rows(0).Item(0) + 1).ToString()
            End If

            .OpenMode = "ADD"
            .ShowDialog()
        End With
    End Sub

    Private Sub cmdExit_Click(sender As Object, e As EventArgs) Handles cmdExit.Click
        'Application.Exitだと例外が発生してしまう…
        Environment.Exit(0)
    End Sub
End Class
