﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModifyAccount
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmModifyAccount))
        Me.txtID1 = New System.Windows.Forms.TextBox()
        Me.lblAccountID = New System.Windows.Forms.Label()
        Me.lblID1Header = New System.Windows.Forms.Label()
        Me.lblAccountIDHeader = New System.Windows.Forms.Label()
        Me.txtID2 = New System.Windows.Forms.TextBox()
        Me.lblID2Header = New System.Windows.Forms.Label()
        Me.txtPassword2 = New System.Windows.Forms.TextBox()
        Me.lblPassword2Header = New System.Windows.Forms.Label()
        Me.txtPassword1 = New System.Windows.Forms.TextBox()
        Me.lblPassword1Header = New System.Windows.Forms.Label()
        Me.txtAccountDescription = New System.Windows.Forms.TextBox()
        Me.lblAccountDescriptionHeader = New System.Windows.Forms.Label()
        Me.cmdCansel = New System.Windows.Forms.Button()
        Me.cmdRegisterAccount = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtID1
        '
        Me.txtID1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtID1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtID1.Location = New System.Drawing.Point(141, 36)
        Me.txtID1.MaxLength = 50
        Me.txtID1.Name = "txtID1"
        Me.txtID1.Size = New System.Drawing.Size(177, 26)
        Me.txtID1.TabIndex = 9
        '
        'lblAccountID
        '
        Me.lblAccountID.AutoSize = True
        Me.lblAccountID.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAccountID.Location = New System.Drawing.Point(137, 9)
        Me.lblAccountID.Name = "lblAccountID"
        Me.lblAccountID.Size = New System.Drawing.Size(59, 19)
        Me.lblAccountID.TabIndex = 8
        Me.lblAccountID.Text = "dummy"
        '
        'lblID1Header
        '
        Me.lblID1Header.AutoSize = True
        Me.lblID1Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblID1Header.Location = New System.Drawing.Point(33, 38)
        Me.lblID1Header.Name = "lblID1Header"
        Me.lblID1Header.Size = New System.Drawing.Size(109, 19)
        Me.lblID1Header.TabIndex = 7
        Me.lblID1Header.Text = "User ID1："
        '
        'lblAccountIDHeader
        '
        Me.lblAccountIDHeader.AutoSize = True
        Me.lblAccountIDHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAccountIDHeader.Location = New System.Drawing.Point(94, 9)
        Me.lblAccountIDHeader.Name = "lblAccountIDHeader"
        Me.lblAccountIDHeader.Size = New System.Drawing.Size(49, 19)
        Me.lblAccountIDHeader.TabIndex = 6
        Me.lblAccountIDHeader.Text = "ID："
        '
        'txtID2
        '
        Me.txtID2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtID2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtID2.Location = New System.Drawing.Point(141, 67)
        Me.txtID2.MaxLength = 50
        Me.txtID2.Name = "txtID2"
        Me.txtID2.Size = New System.Drawing.Size(177, 26)
        Me.txtID2.TabIndex = 11
        '
        'lblID2Header
        '
        Me.lblID2Header.AutoSize = True
        Me.lblID2Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblID2Header.Location = New System.Drawing.Point(33, 69)
        Me.lblID2Header.Name = "lblID2Header"
        Me.lblID2Header.Size = New System.Drawing.Size(109, 19)
        Me.lblID2Header.TabIndex = 10
        Me.lblID2Header.Text = "User ID2："
        '
        'txtPassword2
        '
        Me.txtPassword2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPassword2.Location = New System.Drawing.Point(141, 129)
        Me.txtPassword2.MaxLength = 50
        Me.txtPassword2.Name = "txtPassword2"
        Me.txtPassword2.Size = New System.Drawing.Size(177, 26)
        Me.txtPassword2.TabIndex = 15
        '
        'lblPassword2Header
        '
        Me.lblPassword2Header.AutoSize = True
        Me.lblPassword2Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPassword2Header.Location = New System.Drawing.Point(23, 131)
        Me.lblPassword2Header.Name = "lblPassword2Header"
        Me.lblPassword2Header.Size = New System.Drawing.Size(119, 19)
        Me.lblPassword2Header.TabIndex = 14
        Me.lblPassword2Header.Text = "Password2："
        '
        'txtPassword1
        '
        Me.txtPassword1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPassword1.Location = New System.Drawing.Point(141, 98)
        Me.txtPassword1.MaxLength = 50
        Me.txtPassword1.Name = "txtPassword1"
        Me.txtPassword1.Size = New System.Drawing.Size(177, 26)
        Me.txtPassword1.TabIndex = 13
        '
        'lblPassword1Header
        '
        Me.lblPassword1Header.AutoSize = True
        Me.lblPassword1Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPassword1Header.Location = New System.Drawing.Point(23, 100)
        Me.lblPassword1Header.Name = "lblPassword1Header"
        Me.lblPassword1Header.Size = New System.Drawing.Size(119, 19)
        Me.lblPassword1Header.TabIndex = 12
        Me.lblPassword1Header.Text = "Password1："
        '
        'txtAccountDescription
        '
        Me.txtAccountDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAccountDescription.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtAccountDescription.Location = New System.Drawing.Point(141, 161)
        Me.txtAccountDescription.MaxLength = 255
        Me.txtAccountDescription.Multiline = True
        Me.txtAccountDescription.Name = "txtAccountDescription"
        Me.txtAccountDescription.Size = New System.Drawing.Size(341, 73)
        Me.txtAccountDescription.TabIndex = 17
        '
        'lblAccountDescriptionHeader
        '
        Me.lblAccountDescriptionHeader.AutoSize = True
        Me.lblAccountDescriptionHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAccountDescriptionHeader.Location = New System.Drawing.Point(33, 163)
        Me.lblAccountDescriptionHeader.Name = "lblAccountDescriptionHeader"
        Me.lblAccountDescriptionHeader.Size = New System.Drawing.Size(109, 19)
        Me.lblAccountDescriptionHeader.TabIndex = 16
        Me.lblAccountDescriptionHeader.Text = "コメント："
        '
        'cmdCansel
        '
        Me.cmdCansel.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCansel.Location = New System.Drawing.Point(153, 248)
        Me.cmdCansel.Name = "cmdCansel"
        Me.cmdCansel.Size = New System.Drawing.Size(128, 33)
        Me.cmdCansel.TabIndex = 19
        Me.cmdCansel.Text = "キャンセル(&C)"
        Me.cmdCansel.UseVisualStyleBackColor = True
        '
        'cmdRegisterAccount
        '
        Me.cmdRegisterAccount.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdRegisterAccount.Location = New System.Drawing.Point(17, 247)
        Me.cmdRegisterAccount.Name = "cmdRegisterAccount"
        Me.cmdRegisterAccount.Size = New System.Drawing.Size(128, 33)
        Me.cmdRegisterAccount.TabIndex = 18
        Me.cmdRegisterAccount.Text = "登録(&R)"
        Me.cmdRegisterAccount.UseVisualStyleBackColor = True
        '
        'frmModifyAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 292)
        Me.Controls.Add(Me.cmdCansel)
        Me.Controls.Add(Me.cmdRegisterAccount)
        Me.Controls.Add(Me.txtAccountDescription)
        Me.Controls.Add(Me.lblAccountDescriptionHeader)
        Me.Controls.Add(Me.txtPassword2)
        Me.Controls.Add(Me.lblPassword2Header)
        Me.Controls.Add(Me.txtPassword1)
        Me.Controls.Add(Me.lblPassword1Header)
        Me.Controls.Add(Me.txtID2)
        Me.Controls.Add(Me.lblID2Header)
        Me.Controls.Add(Me.txtID1)
        Me.Controls.Add(Me.lblAccountID)
        Me.Controls.Add(Me.lblID1Header)
        Me.Controls.Add(Me.lblAccountIDHeader)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmModifyAccount"
        Me.Text = "Account Manager / アカウント"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtID1 As TextBox
    Friend WithEvents lblAccountID As Label
    Friend WithEvents lblID1Header As Label
    Friend WithEvents lblAccountIDHeader As Label
    Friend WithEvents txtID2 As TextBox
    Friend WithEvents lblID2Header As Label
    Friend WithEvents txtPassword2 As TextBox
    Friend WithEvents lblPassword2Header As Label
    Friend WithEvents txtPassword1 As TextBox
    Friend WithEvents lblPassword1Header As Label
    Friend WithEvents txtAccountDescription As TextBox
    Friend WithEvents lblAccountDescriptionHeader As Label
    Friend WithEvents cmdCansel As Button
    Friend WithEvents cmdRegisterAccount As Button
End Class
