﻿Public Class frmModifyService
    '閉じるボタンの無効化
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_NOCLOSE As Integer = &H200
            Dim cp As CreateParams = MyBase.CreateParams

            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property

    '呼出元フォームから値を受取るためのプロパティ
    Public Property TargetServiceID As String   '編集対象のID番号
    Public Property OpenMode As String          'フォームを開くモード　"ADD"、"MODIFY"

    'データテーブルの定義
    Private ServiceIDDT As New DataTable()  'tbl_serviceの全レコード

    Private Sub frmModifyService_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'モードにかかわらずサービスIDは呼出元から受け取っている
        Me.lblServiceID.Text = TargetServiceID

        If Me.OpenMode = "ADD" Then
            'フォームのタイトル設定
            Me.Text &= "追加"
        ElseIf Me.OpenMode = "MODIFY" Then
            Dim strSQL As String            'SQL文字列

            strSQL = "SELECT ServiceName,URL,ServiceDescription FROM tbl_service WHERE ServiceID = " &
                        Integer.Parse(TargetServiceID)

            Dim SqlDA As New SqlClient.SqlDataAdapter(strSQL, clsCommonValues.strSqlConnection)

            Try
                SqlDA.Fill(ServiceIDDT)
            Catch ex As System.Data.SqlClient.SqlException
                MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End Try


            'アンパサンド縮減処理
            Dim _clsCommonValues As New clsCommonValues

            Me.txtServiceName.Text = _clsCommonValues.SetAnpersandSingled(ServiceIDDT.Rows(0).Item(0).ToString())
            Me.txtURL.Text = _clsCommonValues.SetAnpersandSingled(ServiceIDDT.Rows(0).Item(1).ToString())
            Me.txtServiceDescription.Text = _clsCommonValues.SetAnpersandSingled(ServiceIDDT.Rows(0).Item(2).ToString())

            'データアダプタ開放
            SqlDA.Dispose()

            'フォームのタイトル設定
            Me.Text &= "修正"
        End If
    End Sub

    Private Sub cmdRegisterSerivice_Click(sender As Object, e As EventArgs) Handles cmdRegisterSerivice.Click
        '各入力項目の最大バイト数定義
        Const SERVICE_NAME_MAX_LEN As Integer = 50
        Const URL_MAX_LEN As Integer = 255
        Const SERVICE_DESCRIPTION_MAX_LEN As Integer = 255

        Dim ServiceNameByteCount As Integer
        Dim URLByteCount As Integer
        Dim ServiceDescriptionByteCount As Integer

        Dim strSQL As String                'SQL文字列

        If Len(Me.txtServiceName.Text) = 0 Then
            MessageBox.Show("サービス名が入力されていません", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Len(Me.txtURL.Text) = 0 Then
            MessageBox.Show("URLが入力されていません", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Strings.Left(Me.txtURL.Text, 7) <> "http://" AndAlso Strings.Left(Me.txtURL.Text, 8) <> "https://" Then
            MessageBox.Show("URLはhttp://またはhttps://で始める必要があります", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim _clsCommonValues As New clsCommonValues

        'アンパサンド二重化処理
        Me.txtServiceName.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtServiceName.Text)
        Me.txtURL.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtURL.Text)
        Me.txtServiceDescription.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtServiceDescription.Text)

        '文字列バイト数カウント
        ServiceNameByteCount = _clsCommonValues.getStringByteCount(Me.txtServiceName.Text)
        URLByteCount = _clsCommonValues.getStringByteCount(Me.txtURL.Text)
        ServiceDescriptionByteCount = _clsCommonValues.getStringByteCount(Me.txtServiceDescription.Text)

        If ServiceNameByteCount > SERVICE_NAME_MAX_LEN Then
            MessageBox.Show("サービス名が長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If URLByteCount > URL_MAX_LEN Then
            MessageBox.Show("URLが長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If ServiceDescriptionByteCount > SERVICE_DESCRIPTION_MAX_LEN Then
            MessageBox.Show("コメントが長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'ここで文字列連結に「+」演算子を使うと正常に動かない
        If Me.OpenMode = "ADD" Then
            strSQL = "INSERT INTO tbl_service (ServiceID,ServiceName,URL,ServiceDescription) VALUES (" &
                        Integer.Parse(TargetServiceID) & ",'" & Me.txtServiceName.Text & "','" & Me.txtURL.Text & "','" &
                        Me.txtServiceDescription.Text & "')"
        ElseIf Me.OpenMode = "MODIFY" Then
            strSQL = "UPDATE tbl_service SET ServiceName = '" & Me.txtServiceName.Text & "',URL = '" & Me.txtURL.Text &
                        "',ServiceDescription = '" & Me.txtServiceDescription.Text & "' WHERE ServiceID = " &
                        Integer.Parse(TargetServiceID)
        End If

        'DB接続した後一度Fillしたデータテーブルに更新をする必要がある
        Dim SqlDA As New SqlClient.SqlDataAdapter(strSQL, clsCommonValues.strSqlConnection)

        Try
            SqlDA.Fill(ServiceIDDT)
            SqlDA.Update(ServiceIDDT)
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)

            Exit Sub
        End Try

        'データアダプタ開放
        SqlDA.Dispose()

        'frmMainを開き直す
        Me.Dispose()

        Dim _frmMain As New frmMain()
        _frmMain.ShowDialog()
    End Sub

    Private Sub cmdCansel_Click(sender As Object, e As EventArgs) Handles cmdCansel.Click
        Me.Close()
    End Sub
End Class