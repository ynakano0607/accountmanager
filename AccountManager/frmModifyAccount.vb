﻿Public Class frmModifyAccount
    '閉じるボタンの無効化
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_NOCLOSE As Integer = &H200
            Dim cp As CreateParams = MyBase.CreateParams

            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property

    '呼出元フォームから値を受取るためのプロパティ
    Public Property TargetServiceID As String   '編集対象のサービスID
    Public Property TargetServiceName As String '編集対象のサービス名
    Public Property TargetAccountID As String   '編集対象のアカウントID
    Public Property OpenMode As String          'フォームを開くモード　"ADD"、"MODIFY"

    'データテーブルの定義
    Private AccountIDDT As New DataTable()  'tbl_accountの全レコード

    Private Sub frmModifyAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'モードにかかわらずサービスIDとサービス名は呼出元から受け取っている
        Me.lblAccountID.Text = TargetAccountID

        If Me.OpenMode = "ADD" Then
            'フォームのタイトル設定
            Me.Text &= "追加(" & TargetServiceName & ")"
        ElseIf Me.OpenMode = "MODIFY" Then
            Dim strSQL As String

            strSQL = "SELECT ID1,ID2,Password1,Password2,AccountDescription FROM tbl_account WHERE ServiceID = " &
                TargetServiceID & " AND AccountID = " & TargetAccountID

            Dim SqlDA As New SqlClient.SqlDataAdapter(strSQL, clsCommonValues.strSqlConnection)

            Try
                SqlDA.Fill(AccountIDDT)
            Catch ex As System.Data.SqlClient.SqlException
                MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End Try

            'アンパサンド縮減処理
            Dim _clsCommonValues As New clsCommonValues

            Me.txtID1.Text = _clsCommonValues.SetAnpersandSingled(AccountIDDT.Rows(0).Item(0).ToString())
            Me.txtID2.Text = _clsCommonValues.SetAnpersandSingled(AccountIDDT.Rows(0).Item(1).ToString())
            Me.txtPassword1.Text = _clsCommonValues.SetAnpersandSingled(AccountIDDT.Rows(0).Item(2).ToString())
            Me.txtPassword2.Text = _clsCommonValues.SetAnpersandSingled(AccountIDDT.Rows(0).Item(3).ToString())
            Me.txtAccountDescription.Text = _clsCommonValues.SetAnpersandSingled(AccountIDDT.Rows(0).Item(4).ToString())

            'データアダプタ開放
            SqlDA.Dispose()

            'フォームのタイトル設定
            Me.Text &= "修正(" & TargetServiceName & ")"
        End If

    End Sub

    Private Sub cmdCansel_Click(sender As Object, e As EventArgs) Handles cmdCansel.Click
        Me.Dispose()
    End Sub

    Private Sub cmdRegisterAccount_Click(sender As Object, e As EventArgs) Handles cmdRegisterAccount.Click
        '各入力項目の最大バイト数定義
        Const ID1_MAX_LEN As Integer = 50
        Const ID2_MAX_LEN As Integer = 50
        Const PASSWORD1_MAX_LEN As Integer = 50
        Const PASSWORD2_MAX_LEN As Integer = 50
        Const ACCOUNT_DESCRIPTION_MAX_LEN As Integer = 255

        Dim ID1ByteCount As Integer
        Dim ID2ByteCount As Integer
        Dim Password1ByteCount As Integer
        Dim Password2ByteCount As Integer
        Dim AccountDescriptionByteCount As Integer

        Dim strSQL As String                'SQL文字列

        If Len(Me.txtID1.Text) = 0 Then
            MessageBox.Show("ユーザID(1)が入力されていません", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Len(Me.txtPassword1.Text) = 0 Then
            MessageBox.Show("パスワード(1)が入力されていません", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim _clsCommonValues As New clsCommonValues

        'アンパサンド二重化処理
        Me.txtID1.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtID1.Text)
        Me.txtID2.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtID2.Text)
        Me.txtPassword1.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtPassword1.Text)
        Me.txtPassword2.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtPassword2.Text)
        Me.txtAccountDescription.Text = _clsCommonValues.SetAnpersandDoubled(Me.txtAccountDescription.Text)

        '文字列バイト数カウント
        ID1ByteCount = _clsCommonValues.getStringByteCount(Me.txtID1.Text)
        ID2ByteCount = _clsCommonValues.getStringByteCount(Me.txtID2.Text)
        Password1ByteCount = _clsCommonValues.getStringByteCount(Me.txtPassword1.Text)
        Password2ByteCount = _clsCommonValues.getStringByteCount(Me.txtPassword2.Text)
        AccountDescriptionByteCount = _clsCommonValues.getStringByteCount(Me.txtAccountDescription.Text)

        If ID1ByteCount > ID1_MAX_LEN Then
            MessageBox.Show("ユーザID(1)が長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If ID2ByteCount > ID2_MAX_LEN Then
            MessageBox.Show("ユーザID(2)が長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Password1ByteCount > PASSWORD1_MAX_LEN Then
            MessageBox.Show("パスワード(1)が長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Password2ByteCount > PASSWORD2_MAX_LEN Then
            MessageBox.Show("パスワード(2)が長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If AccountDescriptionByteCount > ACCOUNT_DESCRIPTION_MAX_LEN Then
            MessageBox.Show("コメントが長すぎます", "登録内容エラー", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'ここで文字列連結に「+」演算子を使うと正常に動かない
        If Me.OpenMode = "ADD" Then
            strSQL = "INSERT INTO tbl_account (ServiceID,AccountID,ID1,ID2,Password1,Password2,AccountDescription) VALUES (" &
                        Integer.Parse(TargetServiceID) & "," & Integer.Parse(TargetAccountID) & ",'" & Me.txtID1.Text &
                        "','" & Me.txtID2.Text & "','" & Me.txtPassword1.Text & "','" & Me.txtPassword2.Text & "','" &
                        Me.txtAccountDescription.Text & "')"
        ElseIf Me.OpenMode = "MODIFY" Then
            strSQL = "UPDATE tbl_account SET ID1 = '" & Me.txtID1.Text & "',ID2 = '" & Me.txtID2.Text &
                        "',Password1 = '" & Me.txtPassword1.Text & "',Password2 = '" & Me.txtPassword2.Text &
                        "',AccountDescription = '" & Me.txtAccountDescription.Text & "' WHERE ServiceID = " &
                        Integer.Parse(TargetServiceID) & " AND AccountID = " & Integer.Parse(TargetAccountID)
        End If

        'DB接続した後一度Fillしたデータテーブルに更新をする必要がある
        Dim SqlDA As New SqlClient.SqlDataAdapter(strSQL, clsCommonValues.strSqlConnection)

        Try
            SqlDA.Fill(AccountIDDT)
            SqlDA.Update(AccountIDDT)
        Catch ex As System.Data.SqlClient.SqlException
            MessageBox.Show(ex.Message & vbCrLf & "エラー番号： " & ex.Number, "データベースエラー",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

        'データアダプタ開放
        SqlDA.Dispose()

        'frmShowAccountを開き直す
        Me.Dispose()

        Dim _frmShowAccount As New frmShowAccount()

        With _frmShowAccount
            .TargetServiceID = TargetServiceID
            .ShowDialog()
        End With
    End Sub
End Class