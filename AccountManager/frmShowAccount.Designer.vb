﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmShowAccount
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmShowAccount))
        Me.lblServiceDescriptionHeader = New System.Windows.Forms.Label()
        Me.lblURLHeader = New System.Windows.Forms.Label()
        Me.lblServiceNameHeader = New System.Windows.Forms.Label()
        Me.lblServiceIDHeader = New System.Windows.Forms.Label()
        Me.lblServiceID = New System.Windows.Forms.Label()
        Me.lblServiceName = New System.Windows.Forms.Label()
        Me.lblServiceDescription = New System.Windows.Forms.Label()
        Me.lllURL = New System.Windows.Forms.LinkLabel()
        Me.line1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.lblID1Header = New System.Windows.Forms.Label()
        Me.lblID2Header = New System.Windows.Forms.Label()
        Me.lblPassword1Header = New System.Windows.Forms.Label()
        Me.lblPassword2Header = New System.Windows.Forms.Label()
        Me.lblAccountDescriptionHeader = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.cmdClose = New System.Windows.Forms.Button()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblServiceDescriptionHeader
        '
        Me.lblServiceDescriptionHeader.AutoSize = True
        Me.lblServiceDescriptionHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceDescriptionHeader.Location = New System.Drawing.Point(21, 181)
        Me.lblServiceDescriptionHeader.Name = "lblServiceDescriptionHeader"
        Me.lblServiceDescriptionHeader.Size = New System.Drawing.Size(109, 19)
        Me.lblServiceDescriptionHeader.TabIndex = 7
        Me.lblServiceDescriptionHeader.Text = "コメント："
        '
        'lblURLHeader
        '
        Me.lblURLHeader.AutoSize = True
        Me.lblURLHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblURLHeader.Location = New System.Drawing.Point(73, 86)
        Me.lblURLHeader.Name = "lblURLHeader"
        Me.lblURLHeader.Size = New System.Drawing.Size(59, 19)
        Me.lblURLHeader.TabIndex = 6
        Me.lblURLHeader.Text = "URL："
        '
        'lblServiceNameHeader
        '
        Me.lblServiceNameHeader.AutoSize = True
        Me.lblServiceNameHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceNameHeader.Location = New System.Drawing.Point(4, 49)
        Me.lblServiceNameHeader.Name = "lblServiceNameHeader"
        Me.lblServiceNameHeader.Size = New System.Drawing.Size(129, 19)
        Me.lblServiceNameHeader.TabIndex = 5
        Me.lblServiceNameHeader.Text = "サービス名："
        '
        'lblServiceIDHeader
        '
        Me.lblServiceIDHeader.AutoSize = True
        Me.lblServiceIDHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceIDHeader.Location = New System.Drawing.Point(84, 15)
        Me.lblServiceIDHeader.Name = "lblServiceIDHeader"
        Me.lblServiceIDHeader.Size = New System.Drawing.Size(49, 19)
        Me.lblServiceIDHeader.TabIndex = 4
        Me.lblServiceIDHeader.Text = "ID："
        '
        'lblServiceID
        '
        Me.lblServiceID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblServiceID.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceID.Location = New System.Drawing.Point(125, 14)
        Me.lblServiceID.Name = "lblServiceID"
        Me.lblServiceID.Size = New System.Drawing.Size(260, 29)
        Me.lblServiceID.TabIndex = 8
        Me.lblServiceID.Text = "dummy"
        '
        'lblServiceName
        '
        Me.lblServiceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblServiceName.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceName.Location = New System.Drawing.Point(125, 49)
        Me.lblServiceName.Name = "lblServiceName"
        Me.lblServiceName.Size = New System.Drawing.Size(260, 32)
        Me.lblServiceName.TabIndex = 9
        Me.lblServiceName.Text = "dummy"
        '
        'lblServiceDescription
        '
        Me.lblServiceDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblServiceDescription.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblServiceDescription.Location = New System.Drawing.Point(125, 181)
        Me.lblServiceDescription.Name = "lblServiceDescription"
        Me.lblServiceDescription.Size = New System.Drawing.Size(758, 106)
        Me.lblServiceDescription.TabIndex = 10
        Me.lblServiceDescription.Text = "dummy"
        '
        'lllURL
        '
        Me.lllURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lllURL.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lllURL.Location = New System.Drawing.Point(125, 87)
        Me.lllURL.Name = "lllURL"
        Me.lllURL.Size = New System.Drawing.Size(758, 87)
        Me.lllURL.TabIndex = 11
        Me.lllURL.TabStop = True
        Me.lllURL.Text = "http://"
        '
        'line1
        '
        Me.line1.BorderWidth = 3
        Me.line1.Name = "line1"
        Me.line1.X1 = 8
        Me.line1.X2 = 913
        Me.line1.Y1 = 300
        Me.line1.Y2 = 300
        '
        'lblID1Header
        '
        Me.lblID1Header.AutoSize = True
        Me.lblID1Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblID1Header.Location = New System.Drawing.Point(35, 10)
        Me.lblID1Header.Name = "lblID1Header"
        Me.lblID1Header.Size = New System.Drawing.Size(63, 15)
        Me.lblID1Header.TabIndex = 13
        Me.lblID1Header.Text = "UserID1"
        '
        'lblID2Header
        '
        Me.lblID2Header.AutoSize = True
        Me.lblID2Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblID2Header.Location = New System.Drawing.Point(385, 10)
        Me.lblID2Header.Name = "lblID2Header"
        Me.lblID2Header.Size = New System.Drawing.Size(63, 15)
        Me.lblID2Header.TabIndex = 14
        Me.lblID2Header.Text = "UserID2"
        '
        'lblPassword1Header
        '
        Me.lblPassword1Header.AutoSize = True
        Me.lblPassword1Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPassword1Header.Location = New System.Drawing.Point(35, 25)
        Me.lblPassword1Header.Name = "lblPassword1Header"
        Me.lblPassword1Header.Size = New System.Drawing.Size(79, 15)
        Me.lblPassword1Header.TabIndex = 15
        Me.lblPassword1Header.Text = "Password1"
        '
        'lblPassword2Header
        '
        Me.lblPassword2Header.AutoSize = True
        Me.lblPassword2Header.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPassword2Header.Location = New System.Drawing.Point(385, 25)
        Me.lblPassword2Header.Name = "lblPassword2Header"
        Me.lblPassword2Header.Size = New System.Drawing.Size(79, 15)
        Me.lblPassword2Header.TabIndex = 16
        Me.lblPassword2Header.Text = "Password2"
        '
        'lblAccountDescriptionHeader
        '
        Me.lblAccountDescriptionHeader.AutoSize = True
        Me.lblAccountDescriptionHeader.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAccountDescriptionHeader.Location = New System.Drawing.Point(35, 40)
        Me.lblAccountDescriptionHeader.Name = "lblAccountDescriptionHeader"
        Me.lblAccountDescriptionHeader.Size = New System.Drawing.Size(95, 15)
        Me.lblAccountDescriptionHeader.TabIndex = 17
        Me.lblAccountDescriptionHeader.Text = "Description"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblServiceDescription)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblServiceID)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblServiceIDHeader)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblServiceName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblServiceNameHeader)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lllURL)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblURLHeader)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblServiceDescriptionHeader)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ShapeContainer2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblAccountDescriptionHeader)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblPassword2Header)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblID1Header)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblID2Header)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblPassword1Header)
        Me.SplitContainer1.Size = New System.Drawing.Size(920, 661)
        Me.SplitContainer1.SplitterDistance = 303
        Me.SplitContainer1.TabIndex = 18
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.line1})
        Me.ShapeContainer2.Size = New System.Drawing.Size(920, 303)
        Me.ShapeContainer2.TabIndex = 12
        Me.ShapeContainer2.TabStop = False
        '
        'cmdClose
        '
        Me.cmdClose.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdClose.Location = New System.Drawing.Point(25, 667)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(128, 33)
        Me.cmdClose.TabIndex = 19
        Me.cmdClose.Text = "閉じる(&C)"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'frmShowAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(929, 712)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.SplitContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmShowAccount"
        Me.Text = "Account Manager / "
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblServiceDescriptionHeader As Label
    Friend WithEvents lblURLHeader As Label
    Friend WithEvents lblServiceNameHeader As Label
    Friend WithEvents lblServiceIDHeader As Label
    Friend WithEvents lblServiceID As Label
    Friend WithEvents lblServiceName As Label
    Friend WithEvents lblServiceDescription As Label
    Friend WithEvents lllURL As LinkLabel
    Friend WithEvents line1 As PowerPacks.LineShape
    Friend WithEvents lblID1Header As Label
    Friend WithEvents lblID2Header As Label
    Friend WithEvents lblPassword1Header As Label
    Friend WithEvents lblPassword2Header As Label
    Friend WithEvents lblAccountDescriptionHeader As Label
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents ShapeContainer2 As PowerPacks.ShapeContainer
    Friend WithEvents cmdClose As Button
End Class
